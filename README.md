# Deep Metric Learning

This is the deep metric learning exercise for FL8 week 5 day 3 by Gilad Rosenblatt.

### Run

Run `main.py` from `code` as working directory.

## License

[WTFPL](http://www.wtfpl.net/)
