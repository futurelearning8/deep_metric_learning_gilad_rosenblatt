import tensorflow as tf
import numpy as np
from time import time
from generators import TrainSet, ValidationSet
from tensorflow.python.keras.callbacks import TensorBoard
from model import build_siamese_model


def main():
    # Get model.
    model = build_siamese_model(use_l2_distance=True)
    model.summary()

    # Load dataset generators.
    batch_size = 64
    num_positives = 32
    train_ds = TrainSet(batch_size=batch_size, num_positives=num_positives)
    test_ds = ValidationSet(batch_size=batch_size, num_positives=num_positives).get(num_batches=8)
    x_test, y_test = test_ds
    x_test = np.einsum("ikjl->ijkl", x_test)

    # Define optimizer.
    learning_rate = 0.001
    optimizer = tf.keras.optimizers.Adam(
        learning_rate=learning_rate
    )

    # Create a callback to save model weights every epoch (include epoch in filename).
    save_checkpoint = tf.keras.callbacks.ModelCheckpoint(
        filepath="../training/cp-{epoch:03d}-{val_loss:.2f}.ckpt",
        verbose=1,
        save_weights_only=True,
        save_best_only=True  # Latest checkpoint should be best since it is reloaded at the end.
    )

    # Create a callback to reduce learning rate on plateau.
    reduce_learning_rate = tf.keras.callbacks.ReduceLROnPlateau(
        monitor="val_loss",
        factor=0.2,
        patience=5,
        min_lr=learning_rate / 100
    )

    # Compile the model and optimizer.
    model.compile(
        loss=tf.keras.losses.BinaryCrossentropy(),
        optimizer=optimizer,
        metrics=["accuracy"]
    )

    # Train the model w/callbacks.
    tensorboard = TensorBoard(f"../logs/{time()}")
    epochs = 40
    steps_per_epoch = 124
    model.fit(
        x=train_ds,
        validation_data=(x_test, y_test),
        steps_per_epoch=steps_per_epoch,
        epochs=epochs,
        callbacks=[save_checkpoint, reduce_learning_rate, tensorboard],
        verbose=1
    )

    # Evaluate trained model on validation set.
    loss, accuracy = model.evaluate(x=x_test, y=y_test, verbose=2)
    print(f"Model accuracy on validation set: {100 * accuracy:5.2f}%")

    # Save the model.
    model_path = "../models/my_model"
    model.save(
        f"{model_path}_"
        f"bsz{batch_size}_"
        f"pos{num_positives}_"
        f"spe{steps_per_epoch}_"
        f"epo{epochs}_"
        f"acc{accuracy:.3f}"
    )

    # Load weights from latest (best) checkpoint and evaluate again.
    model.load_weights(tf.train.latest_checkpoint("../training"))
    loss, accuracy = model.evaluate(x=x_test, y=y_test, verbose=2)
    print(f"Model accuracy on validation set: {100 * accuracy:5.2f}% (with loss = {loss})")

    # Save the model from the best checkpoint.
    model_path = "../models/my_model"
    model.save(
        f"{model_path}_"
        f"bsz{batch_size}_"
        f"pos{num_positives}_"
        f"spe{steps_per_epoch}_"
        f"epo{epochs}_"
        f"ckp_"
        f"acc{accuracy:.3f}"
    )


def load_best_model_and_score():
    # Load validation dataset.
    test_ds = ValidationSet(batch_size=64, num_positives=32).get(num_batches=8, random_state=42)
    x_test, y_test = test_ds
    x_test = np.einsum("ikjl->ijkl", x_test)

    # Load saved trained model (epoch 9 checkpoint has best validation score).
    filename = \
        f"{'../models/my_model'}_"\
        f"bsz{64}_"\
        f"pos{32}_"\
        f"spe{124}_"\
        f"epo{40}_"\
        f"ckp_"\
        f"acc{0.803:.3f}"
    model = tf.keras.models.load_model(filename)
    model.summary()

    # Evaluate trained model on validation set.
    loss, accuracy = model.evaluate(x=x_test, y=y_test, verbose=2)
    print(f"Model accuracy on validation set: {100 * accuracy:5.2f}% (with loss = {loss})")


if __name__ == "__main__":
    # main()
    load_best_model_and_score()
