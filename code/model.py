import tensorflow as tf
from distances import l2_distance


def build_embedding_model():
    """Build the embedding model from a (64, 128, 3) image to a feature space."""

    # Define input.
    inputs = tf.keras.Input(shape=(64, 128, 3), name="input")
    x = inputs

    # Add convolutional and pooling blocks.
    block_filters = [64, 128, 256, 512]
    block_repetitions = [2, 2, 3, 3]
    for num_block, (filters, repetitions), in enumerate(zip(block_filters, block_repetitions)):

        # Add convolutional + batch normalization + activation layers.
        for num_conv in range(repetitions):
            x = tf.keras.layers.Conv2D(
                filters=filters,
                kernel_size=(3, 3),
                padding="same",
                name=f"block_{num_block + 1}_conv_{num_conv + 1}"
            )(x)
            x = tf.keras.layers.BatchNormalization(
                name=f"block_{num_block + 1}_batch_norm_{num_conv + 1}"
            )(x)
            x = tf.keras.layers.Activation(
                activation=tf.nn.relu,
                name=f"block_{num_block + 1}_relu_{num_conv + 1}"
            )(x)

        # Add pooling layer.
        x = tf.keras.layers.MaxPooling2D(
            pool_size=(2, 2),
            strides=(2, 2),
            name=f"block_{num_block + 1}_pooling"
        )(x)

    # Transition to a flattened layer: add global average pooling and flatten.
    x = tf.keras.layers.GlobalAveragePooling2D(name="global_pooling")(x)
    x = tf.keras.layers.Flatten(name="flatten")(x)

    # Add dropout at last layer.
    x = tf.keras.layers.Dropout(rate=0.3, name="dropout")(x)

    # # Use sigmoid to get class probability.
    outputs = tf.keras.layers.Dense(512, activation=tf.nn.relu, name="fully-connected")(x)

    return tf.keras.models.Model(inputs=[inputs], outputs=[outputs])


def build_siamese_model(use_l2_distance=True):
    """
    Build the siamese model from two (64, 128, 3) images concatenated along the channel dimension via a common feature
    space (embedding layer) to a concatenation and classification for similarity (deep metric learning).
    """

    # Define input.
    inputs = tf.keras.Input(shape=(64, 128, 6), name="combined_input")

    # Split to 2 siamese inputs.
    siamese1 = inputs[..., :3]
    siamese2 = inputs[..., 3:]

    # Build embedding model.
    embedding_model = build_embedding_model()

    # Pass two siamese parts separately to get embeddings.
    embedding1 = embedding_model(siamese1)
    embedding2 = embedding_model(siamese2)

    # Concatenate embeddings in channel dimension and (optionally) calculate distance between embeddings.
    if use_l2_distance:
        x = tf.keras.layers.Lambda(l2_distance)([embedding1, embedding2])
    else:
        x = tf.keras.layers.concatenate([embedding1, embedding2])

    # Build fully-connected network to "learn the distance measure".
    x = tf.keras.layers.Dense(512)(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Activation(activation=tf.nn.relu)(x)
    x = tf.keras.layers.Dense(128)(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Activation(activation=tf.nn.relu)(x)
    x = tf.keras.layers.Dense(32)(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Activation(activation=tf.nn.relu)(x)
    x = tf.keras.layers.Dense(8)(x)
    x = tf.keras.layers.BatchNormalization()(x)
    x = tf.keras.layers.Activation(activation=tf.nn.relu)(x)

    # Use sigmoid to get class probability.
    outputs = tf.keras.layers.Dense(1, activation=tf.nn.sigmoid, name="classification_layer")(x)

    return tf.keras.models.Model(inputs=[inputs], outputs=[outputs])
