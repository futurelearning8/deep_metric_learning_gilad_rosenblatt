import os
import random
import cv2
import numpy as np
import matplotlib.pyplot as plt


class BatchGenerator:
    """Generator of random image pair batches of given size and fixed number of positives per batch."""

    # Parent directory where image dataset is stored.
    PARENT_DIR = "../dataset_ETHZ"

    # Size to resize images to in the opencv format of (width, height).
    RESIZE_SHAPE = (64, 128)

    def __init__(self, sequences, batch_size=2, num_positives=1, debug_mode=False):
        """
        Set image sequences (sub-folders) to run from and the debug state.

        :param list sequences: list of sub-folder name strings indicating sequences to pool images from.
        :param int batch_size: number of image pairs to include in a single batch.
        :param int num_positives: number of positive (similar) image pairs in a batch.
        :param bool debug_mode: if True plot each batches with labels.
        """
        assert num_positives <= batch_size
        self.sequences = sequences
        self.debug_mode = debug_mode
        self.batch_size = batch_size
        self.num_positives = num_positives
        self.composition = np.full(shape=(batch_size,), fill_value=False)
        self.composition[:num_positives] = True

    def __iter__(self):
        """Start iteration. Nothing to do as this iterable does not exhaust."""
        return self

    def __next__(self):
        """Return next image pair batch (image1, image2), label keeping a fixed number of positives per batch."""

        # Instantiate empty arrays of the right size and data type.
        batch = np.zeros(shape=(self.batch_size, *reversed(self.RESIZE_SHAPE), 6), dtype=np.float32)
        labels = np.zeros(shape=(self.batch_size, 1), dtype=int)

        # Fill batch image-pair by image-pair (normalize values to 0-1).
        for index, same_person in enumerate(self.composition):
            images = BatchGenerator.get_image_pair(same_person=same_person, sequences=self.sequences)
            batch[index, :, :, :] = np.dstack(images) / 255
            labels[index, 0] = 1 if same_person else 0

        # Plot the batch if in debug mode.
        if self.debug_mode:
            self.plot(batch, labels)

        # Return the image-pair batch and labels in a tensorflow compatible format.
        return batch, labels

    @staticmethod
    def get_image_pair(same_person, sequences):
        """
        Randomly select and load a pair of images, of either different or the same person, from the training sequences.

        :param bool same_person: if True returns two images of the same person (positive sample).
        :param list sequences: list of sequences to pool images from (sub-folders of the parent folder).
        :return tuple: BGR images of dimensions RESIZE_SHAPE = (width, height, 3)
        """

        # Choose one image sequence.
        sequence = random.choice(sequences)
        sequence_dir = os.path.join(BatchGenerator.PARENT_DIR, sequence)

        # Choose one person and a second (same or different) person.
        if not same_person:
            persons = random.sample(os.listdir(sequence_dir), k=2)
        else:
            persons = [random.choice(os.listdir(sequence_dir))] * 2
        person_dirs = [os.path.join(sequence_dir, person) for person in persons]

        # Choose one image per person
        filenames = [random.choice(os.listdir(person_dir)) for person_dir in person_dirs]
        file_paths = [os.path.join(person_dir, filename) for person_dir, filename in zip(person_dirs, filenames)]

        # Return the two selected images.
        return tuple(cv2.resize(cv2.imread(path), dsize=BatchGenerator.RESIZE_SHAPE) for path in file_paths)

    # FIXME plot with RGB and not BGR channels.
    @staticmethod
    def plot(batch, labels):
        """
        :param np.ndarray batch: 2-pair batch shaped (batch_size, height, width, 6) with stacked channels for each pair.
        :param np.ndarray labels: 0-1 labels for each pair of shape (batch_size, 1).
        """
        batch_size = labels.shape[0]
        fig, axes = plt.subplots(nrows=1, ncols=batch_size, figsize=(10, 7))
        for index, (ax, label) in enumerate(zip(axes, labels.ravel())):
            ax.imshow(np.hstack((batch[index, :, :, :3], batch[index, :, :, 3:])))
            ax.set_xticks([])
            ax.set_yticks([])
            ax.set_title(f"label {label}")
        plt.show()


class TrainSet(BatchGenerator):
    """Generator of random pair batches from train sequences."""

    def __init__(self, *args, **kwargs):
        """Initialize random image-pair batch generator from train sequences."""
        super().__init__(sequences=["seq1", "seq2"], *args, **kwargs)


class DatasetLoader:
    """Build a data set by random but repeatable pooling and load it to memory in full."""

    def __init__(self, sequences, batch_size=2, num_positives=1, debug_mode=False):
        """
        Initialize a dataset loader for image pairs from input sequences with total number of samples corresponding
        to input number of batches (there is no real division into batches, it just sets the total number of samples).

        :param list sequences: list of sub-folder string names containing images sequences to pool from.
        :param int batch_size: number of image pairs to include in a single batch.
        :param int num_positives: number of positive (similar) image pairs in a batch.
        :param bool debug_mode: if True plot each batches with labels.
        :return tuple: numpy arrays in the same format as the training set, but not as a generator.
        """
        self.batch_size = batch_size
        self.num_positives = num_positives
        self.sequences = sequences
        self.debug_mode = debug_mode

    def get(self, num_batches, random_state=42):
        """
        Build a random but repeatable data set of image pairs from input sequences with total number of
        samples corresponding to input number of batches (there is no real division into batches).

        :param num_batches: number of batches in the dataset.
        :param int random_state: seed to use for random library before randomly choosing image pairs for dataset.
        :return tuple: numpy arrays in the same format as the training set, but not as a generator.
        """

        # Instantiate empty arrays of the right size and data type.
        x = np.zeros(
            shape=(self.batch_size * num_batches, *reversed(BatchGenerator.RESIZE_SHAPE), 6),
            dtype=np.float32
        )
        y = np.zeros(
            shape=(self.batch_size * num_batches, 1),
            dtype=int
        )

        # Set 0-1 composition in each "batch" of the dataset.
        composition = np.full(shape=(self.batch_size,), fill_value=False)
        composition[:self.num_positives] = True

        # Fill dataset batch-by-batch with random pairs selected from input sequence adhering to 0-1 batch composition.
        random.seed(random_state)
        for batch_num in range(num_batches):
            for pair_num, same_person in enumerate(composition):
                index = batch_num * self.batch_size + pair_num
                images = BatchGenerator.get_image_pair(same_person=same_person, sequences=self.sequences)
                x[index, :, :, :] = np.dstack(images) / 255
                y[index, 0] = 1 if same_person else 0

            # Plot the batch if in debug mode.
            if self.debug_mode:
                BatchGenerator.plot(
                    x[batch_num * self.batch_size:(batch_num + 1) * self.batch_size, :, :, :],
                    y[batch_num * self.batch_size:(batch_num + 1) * self.batch_size, 0]
                )

        # Return dataset.
        return x, y


class ValidationSet(DatasetLoader):
    """Randomly selected but repeatable validation set."""

    def __init__(self, *args, **kwargs):
        """Set sequence to validation set."""
        super().__init__(sequences=["seq3"], *args, **kwargs)

    @staticmethod
    def save():
        """Load dataset and save it to binary files and save dataset as binary."""
        x_test, y_test = ValidationSet(batch_size=64, num_positives=32).get(num_batches=8, random_state=42)
        np.save('../data/test_ds_x.npy', x_test)
        np.save('../data/test_ds_y.npy', y_test)


def main():
    train_set = TrainSet(batch_size=6, num_positives=3, debug_mode=True)
    x, y = next(train_set)
    x, y = ValidationSet(batch_size=2, num_positives=1, debug_mode=True).get(num_batches=2)


if __name__ == '__main__':
    main()
