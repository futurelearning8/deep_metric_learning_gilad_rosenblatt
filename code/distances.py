import tensorflow as tf


def l2_distance(x_and_y):
    """Calculate the element-wise L2 distance between two flattened concatenated tensors."""
    x, y = x_and_y
    return tf.math.square(x - y)
